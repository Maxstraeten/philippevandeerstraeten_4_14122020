
// ScrollTop
$(document).ready(function(){
	$(window).scroll(function () {
			if ($(this).scrollTop() > 50) {
				$('#back-to-top').fadeIn();
			} else {
				$('#back-to-top').fadeOut();
			}
		});
		// scroll body to 0px on click
		$('#back-to-top').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 400);
			return false;
		});
});

// debut ligthbox2 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    lightbox.option({
		'alwaysShowNavOnTouchDevices': false, // Default
		'albumLabel': "Image %1 of %2",  // Default
		'disableScrolling': false, // Default
		'fadeDuration': 600, // Default
		'fitImagesInViewport': true, // Default
		'imageFadeDuration': 600, // Default
		// 'maxWidth':,
		// 'maxHeight':,
		'positionFromTop': 50, // Default
		'resizeDuration': 700, // Default
		'showImageNumberLabel': false,
		'wrapAround': true
	  });

	// Bootstrap validate xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
	// bootstrapValidate('#name', 'rule', function (isValid) {
	// 	if (isValid) {
	// 		alert('Élément valide');
	// 	} else {
	// 		alert('Élément non valide');
	// 	}
	//  });

	// bootstrapValidate('#email', 'email:Entrez une adresse E-Mail valide');

	// Example starter JavaScript for disabling form submissions if there are invalid fields
	(function() {
		'use strict';
		window.addEventListener('load', function() {
		// Fetch all the forms we want to apply custom Bootstrap validation styles to
		var forms = document.getElementsByClassName('needs-validation');
		// Loop over them and prevent submission
		var validation = Array.prototype.filter.call(forms, function(form) {
			form.addEventListener('submit', function(event) {
			if (form.checkValidity() === false) {
				event.preventDefault();
				event.stopPropagation();
			}
			form.classList.add('was-validated');
			}, false);
		});
		}, false);
	})();
