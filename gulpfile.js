const fiBers = require('fibers');
const {src, dest, lastRun, series, parallel, watch} = require('gulp');
const gulpSass = require('gulp-sass');
const gulpOrder = require('gulp-order');
const gulpIf = require('gulp-if')
const postCss = require('gulp-postcss');
const purgeCss = require('gulp-purgecss');
const conCat = require('gulp-concat');
const gulpUglify = require('gulp-uglify');
const imageResize = require('gulp-image-resize');
const imageMin = require('gulp-imagemin');
const imageminOptipng = require('imagemin-optipng');
const imageminMozjpeg = require('imagemin-mozjpeg');
const imageminJpegtran = require('imagemin-jpegtran');
const imageminWebp = require('imagemin-webp');
const reName = require('gulp-rename');
const cssComb = require('gulp-csscomb');
const cssBeautify = require('gulp-cssbeautify');
const htmlMin = require('gulp-htmlmin');
const del = require('del');
const fontMin = require('gulp-fontmin');
const fontsWoff2 = require('gulp-ttf2woff2');
const surge = require('gulp-surge');
const browserSync = require('browser-sync').create();
gulpSass.compiler = require('sass');

// MINIFIER HTML XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
function htmlProd(){
    return src('*.html')
    .pipe(htmlMin({
        collapseWhitespace: true,
        removeComments: true
    }))
    .pipe(dest('dist'));
}

// CSS XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
// AMELIORER CSS style et présentation
function cssTop(){
    return src([
        'node_modules/bootstrap/dist/css/bootstrap.min.css',
        'node_modules/lightbox2/src/css/lightbox.css',
        ])
        .pipe(dest('src/css'));
}

function cssTask(){
    const plugins = [
        require('postcss-font-magician'),
        require('autoprefixer')
    ]
    return src('./src/scss/*.scss', {sourcemaps:true})
        .pipe(gulpSass({fiber: fiBers}).on('error', gulpSass.logError))
        .pipe(postCss(plugins))
        .pipe(cssComb())
        .pipe(cssBeautify({
            indent: '  ',
            openbrace: 'separate-line',
            autosemicolon: false
        }))
        .pipe(dest('./src/css', {sourcemaps: "."}));
    }

// MINIFIER & CONCACTER CSS
function cssProd(){
    const plugins = [
        require('cssnano')({
            preset: ['default',{
                svgo: false,
                discardComments:{removeAll: true,},
            }]
        }),
    ]
    return src('./src/css/*.css')
        .pipe(purgeCss({
            content: ['*.html', './src/js/lightbox.min.js'],
            safelist: ['no-webp', 'webp']
        }))
        .pipe(conCat('styles.css'))
        .pipe(postCss(plugins))
        .pipe(dest('./dist/src/css'));
}

// JAVASCRIPT XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
// RASSEMBLEMENT FICHIERS JAVASCRIPT 
function jsTop(){
    return src([
        'node_modules/jquery/dist/jquery.min.js',
        'node_modules/bootstrap/dist/js/bootstrap.bundle.min.js',
        'node_modules/touchswipe/index.js',
        'node_modules/bootstrap-validate/dist/bootstrap-validate.js',
        'node_modules/lightbox2/dist/js/lightbox.min.js'])
    .pipe(dest('./src/js'));
}

function jsTask(){
    return src('./src/js/*.js')
        .pipe(gulpOrder([
            "modernizr-custom.js",
            "jquery.min.js",
            "bootstrap.bundle.min.js",
            "lightbox.min.js",
            "blocs.js",
            "bootstrap-validate.js",
            "index.js",
        ]))
        .pipe(conCat('all.js'))
        .pipe(gulpUglify(
            {
            compress: true,
            mangle: false,
            output: {
                beautify: false,
                comments: false
            }}
        ))
        .pipe(dest('./dist/src/js'));
}

// IMAGES XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
// MONTER IMAGES 
function obtTask(){
    return src('./node_modules/lightbox2/dist/images/*.{png,gif}', {since: lastRun(obtTask)})
    .pipe(dest('./src/img'));
}

// CONVERTIR IMAGES bmp en jpg
function conv1Task(){
    return src('./vendor/img/*.bmp', {since : lastRun(conv1Task)})
    .pipe(imageResize({format:'jpeg'}))
    .pipe(reName(function(path){
        path.extname = ".jpg";
    }))
    .pipe(dest('./src/img'));
}
function conv2Task(){
    return src('./vendor/img/*.{png,jpg}', {since : lastRun(conv2Task)})
    .pipe(dest('./src/img'));
}

// REDIMENSIONNER & PREFIXER IMAGES

function imageTask(done){
    const sizes = [
        { width: 576, suffix: '-sm' },
        { width: 960, suffix: '-lg' },
        { width: 1140, suffix: '-xl' },
    ];
    sizes.forEach(function(size) {
        return src('./src/img/*-fd.jpg', {since: lastRun(imageTask)})
        .pipe(imageResize({
            width: size.width,
            height: 0,
            upscale: false
        }))
        .pipe(reName(function(path) {
         path.basename += size.suffix;
        }))
        .pipe(dest('./src/img'));
    })
    done();
    }

    function imagePort(){
        return src('./src/img/{1..20}.jpg', {since: lastRun(imagePort)})
        .pipe(imageResize({
            width: 320,
            height: 0,
            upscale: false
        }))
        .pipe(reName(function(path){
            path.basename +=  "-portfolio";
        }))
        .pipe(dest('./src/img'));
    }

// MINIFIER IMAGES
function imageProd(){
    return src('./src/img/*.{jpg,png,gif}')
    .pipe(imageMin([
        imageminMozjpeg({quality: 40, progressive: true, dcScanOpt: 2}),
        imageminOptipng({optimizationLevel: 4})
    ],
    {verbose: true}))
    .pipe(dest('./dist/src/img'));
}

function imageWebp(){
    return src('./src/img/*.{jpg,png,gif}')
    .pipe(imageMin([
        imageminWebp({quality: 50}),
    ],
    {verbose: true}))
    .pipe(reName(function(path){
        path.extname = ".webp";
    }))
    .pipe(dest('./dist/src/img'));
}

// FONTS  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
const condition1 =  "et-line.ttf";
function fontTop(){
    return src([
    'node_modules/@fortawesome/fontawesome-free/webfonts/fa-brands-400.ttf',
    'node_modules/@fortawesome/fontawesome-free/webfonts/fa-solid-900.ttf',
    'node_modules/et-line/fonts/et-line.ttf'
    ])
    .pipe(fontsWoff2())
    .pipe(gulpIf(condition1, fontMin()))
    .pipe(dest('./src/webfonts'));
}

function fontsProd(){
    return src('./src/webfonts/*.woff2')
    .pipe(dest('./dist/src/webfonts'));
}

// FilesTask xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
const condition = 'favicon.jpg';
function filesTask(){
    return src(['favicon.jpg', 'robots.txt', 'sitemap.xml'])
    .pipe(gulpIf(condition, imageMin([
        imageminMozjpeg({quality: 50, progressive: true, dcScanOpt: 2}),
    ],
    {verbose: true})))
    .pipe(dest('dist'));
}

// AUTRES XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
// BROWSER SYNC
function browserSyncServe(cb){
    browserSync.init({
        server:{ baseDir: "./"}
        // ou proxy: "yourlocal.dev",
        // port: 3000
    })
    cb();
}

function browserSyncReload(cb){
    browserSync.reload()
    cb();
}

// WATCH ELEMENTS
function watchTask(){
    watch("*.html", browserSyncReload);
    watch("src/js/*.js", browserSyncReload);
    watch("src/scss/*.scss", cssTask, browserSyncReload);
}

// CLEAN dossier dist
function cleanTask(){
    return del(['dist/favicon.jpg', 'dist/robots.txt', 'dist/sitemap.xml', 'dist/*.html', 'dist/src/css/*.css', 'dist/src/js/*.js', 'dist/src/webfonts/*', 'dist/src/img/*']);
}

// DEPLOIEMENT SURGE
function deploy(){
    return surge({
        project: './dist',
        domain: 'la-chouette-agence.surge.sh'
    });
}

// Script d'exportation
exports.create = series(obtTask, conv1Task, conv2Task, parallel(cssTask, jsTask));
exports.createImg = series(obtTask, conv1Task, conv2Task);
exports.creaImg = imageTask;
exports.creaEssai = imagePort;
exports.build = series(htmlProd, parallel(imageProd, cssProd));
exports.tryImg = imageProd;
exports.tryFavicon = filesTask;
exports.try = cssTask;
exports.tryProd = cssProd;
exports.tryJs = jsTask;
exports.tryMe = series(cssTop, jsTop);
exports.tryMinFonts = fontTop;
exports.tryJs= jsTop;
exports.tryCss = cssTop;
exports.watch = parallel(browserSyncServe, watchTask);
exports.clean = cleanTask;
exports.deploy = series(fontsProd, filesTask, htmlProd, jsTask, cssProd, imageProd, imageWebp, deploy);
